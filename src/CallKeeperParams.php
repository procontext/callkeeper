<?php

namespace Procontext\CallKeeper;

use Procontext\CallKeeper\Exception\ValidationException;
use \ReflectionClass;
use \ReflectionProperty;

class CallKeeperParams
{
    /**
     * Название компании, любая число-буквенная строка без спецсимволов
     *
     * @var string
     */
    public $unique;

    /**
     * Ключ сгенерированный в ЛК во вкладке Профиль
     *
     * @var string
     */
    public $apiak;

    /**
     * Hash виджета, из которого будут взяты необходимые параметры для звонка,
     * можно получить на странице редактирования виджета в ЛК
     *
     * @var string
     */
    public $whash;

    /**
     * Временная зона в формате IANA Time Zone Database(Europe/Moscow) или в формате ±hhmm("UTC+03:00")
     *
     * @var string
     */
    public $utc = 'Europe/Moscow';

    /**
     * Уникальный идентификатор пользователя для отправки в Google Analytics
     *
     * @var string
     */
    public $ga_client_id ;

    /**
     * Название инструмента с которого был заказан звонок
     *
     * @var string
     */
    public $tool_name;

    /**
     * Ip адрес клиента
     *
     * @var string
     */
    public $ip_client;

    /**
     * Содержит сведения о адресе сайта с которого произошел переход.
     * Для удобства сюда можно просто скопировать содержимое js переменной document.referrer
     *
     * @var string
     */
    public $referrer;

    /**
     * Номер телефона клиента, на который поступит звонок
     *
     * @var string
     */
    public $client;

    /**
     * Номер телефона компании, на который поступит звонок,
     * либо порядковый номер офиса, указанного в настройках виджета (важно: нумерация начинается с 0)
     *
     * @var string|integer
     */
    public $manager = 0;

    /**
     * Произвольный текст, который будет проговариваться в начале каждого звонка
     *
     * @var string
     */
    public $text_to_manager;

    /**
     * Название сайта, которое будет отображено в информации о звонке (host + path)
     *
     * @var string
     */
    public $site;

    /**
     * Источник перехода по рекламе
     *
     * @var string
     */
    public $utm_source;

    /**
     * Тип трафика
     *
     * @var string
     */
    public $utm_medium;

    /**
     * Обозначение рекламной кампании
     *
     * @var string
     */
    public $utm_campaign;

    /**
     * Cодержание кампании
     *
     * @var string
     */
    public $utm_content;

    /**
     * Условие поиска кампании
     *
     * @var string
     */
    public $utm_term;

    /**
     * Адрес перехода пользователя (schema + host + path)
     *
     * @var string
     */
    public $entry_point;

    /**
     * Название и версия Браузера и ОС клиента
     *
     * @var string
     */
    public $user_agent;

    /**
     * Идентификатор сессии Calltouch.
     * С помощью него Calltouch присвоит переданной заявке источник перехода на сайт посетителя, отправившего ее
     *
     * @var string
     */
    public $calltouch_session_id = '';

    protected $properties;
    protected $errors;

    public function __construct(array $formData)
    {
        if(!$this->apiak = env('CK_APIAK')) {
            $this->errors[] = 'Ключ API не задан в конфигурации';
        }

        if(!$this->whash = env('CK_WHASH')) {
            $this->errors[] = 'Hash виджета не задан в конфигурации';
        }

        $ssl = env('SSL', false);
        if(!$url = env('URL')) {
            $this->errors[] = 'Url не задан в конфигурации';
        } else {
            $this->unique = $url;
            $this->site = $url;
            $this->entry_point = ($ssl ? 'https://' : 'http://') . $url;
            $this->text_to_manager = 'Посетитель заполнил форму на сайте ' . $url;
            $this->tool_name = 'Форма на сайте ' . $url;
        }

        $formData = $this->preload($formData);
        $reflection = new ReflectionClass(get_called_class());
        $this->properties = array_column($reflection->getProperties(ReflectionProperty::IS_PUBLIC), 'name');
        foreach ($this->properties as $property) {
            if (isset($formData[$property]) && !in_array($property, ['tool_name'])) {
                $this->{$property} = $formData[$property];
            } elseif(
                !in_array(
                    $property,
                    [
                        'apiak',
                        'whash',
                        'unique',
                        'site',
                        'entry_point',
                        'text_to_manager',
                        'tool_name' 
                    ]
                ) 
                && !isset($this->{$property})
            ) {
                $this->errors[] = "Параметр " . $property . " не задан";
            }
        }

        if($this->errors) {
            throw new ValidationException($this->errors);
        }
    }

    protected function preload(array $formData): array
    {
        $formData['ip_client'] = isset($formData['ip']) ? $formData['ip'] : null;
        $formData['client'] = isset($formData['phone']) ? $formData['phone'] : null;
        $formData['manager'] = isset($formData['manager_phone']) ? $formData['manager_phone'] : 0;
        return $formData;
    }

    public function export(): array
    {
        $formData = [];
        foreach ($this->properties as $property) {
            $formData[$property] = $this->{$property};
        }
        return $formData;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}
