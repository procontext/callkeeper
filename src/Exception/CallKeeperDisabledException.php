<?php

namespace Procontext\CallKeeper\Exception;

use Throwable;

class CallKeeperDisabledException extends CallKeeperException
{
    public function __construct($message = "CallKeeper API отключен", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}